const dts = require('rollup-plugin-dts').default;
const esbuild = require('rollup-plugin-esbuild').default;
const packageJson = require('./package.json');

const name = packageJson.main.replace(/\.js$/, '');

const bundle = (config) => ({
	...config,
	input: './EventEmitter.ts',
	external: (id) => !/^[./]/.test(id)
});

module.exports = [
	bundle({
		plugins: [esbuild()],
		output: [
			{
				file: `${name}.js`,
				format: 'cjs',
				sourcemap: false
			},
			{
				file: `${name}.mjs`,
				format: 'es',
				sourcemap: false
			}
		]
	}),
	bundle({
		plugins: [dts()],
		output: {
			file: `${name}.d.ts`,
			format: 'es'
		}
	})
];
