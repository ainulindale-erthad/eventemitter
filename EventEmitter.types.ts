export type EventKey = string | symbol;
export type EventsMap = Record<EventKey, any>;
export type EventListener<T extends EventsMap, K extends keyof T> = (data: T[K] extends undefined ? undefined : T[K], event?: K) => void;
export type EventListeners<T extends EventsMap, K extends keyof T> = Record<keyof T, EventListener<T, K>[]>;
export type EventPayloadArgs<T, K extends keyof T = keyof T> = T[K] extends undefined ? [undefined?] : [T[K]];
