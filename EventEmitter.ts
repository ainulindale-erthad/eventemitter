import type { EventListeners, EventListener, EventKey, EventsMap, EventPayloadArgs } from './EventEmitter.types';

export const AnyEvent = Symbol('*');
export const debugKey = Symbol('Debug');

const eventListeners = Symbol('EventKeyListeners');
const onceEventListeners = Symbol('OnceEventListeners');
const anyEventListeners = Symbol('EventListenerAnyEventListeners');
const ignore = Symbol('Ignore');

class EventEmitter<T extends EventsMap = EventsMap> {
	private [eventListeners]: EventListeners<T, EventKey> = {} as EventListeners<T, EventKey>;
	private [onceEventListeners]: EventListeners<T, EventKey> = {} as EventListeners<T, EventKey>;
	private [anyEventListeners]: EventListener<T, EventKey>[] = [];
	private [ignore] = new Set<EventKey>();
	public [debugKey] = false;

	public on<K extends keyof T>(event: K, listener: EventListener<T, K>): void {
		if (event === AnyEvent) {
			this[anyEventListeners].push(listener as EventListener<T, EventKey>);
			return;
		}
		if (!this[eventListeners][event]) this[eventListeners][event] = [];
		this[eventListeners][event].push(listener as EventListener<T, EventKey>);
	}

	public once<K extends keyof T>(event: K, listener: EventListener<T, K>): void {
		if (event === AnyEvent) return;
		if (!this[onceEventListeners][event]) this[onceEventListeners][event] = [];
		this[onceEventListeners][event].push(listener as EventListener<T, EventKey>);
	}

	public off<K extends keyof T>(event: K, listener: EventListener<T, K>): void {
		if (!listener) {
			if (event === AnyEvent) {
				this[anyEventListeners] = [];
				return;
			}

			if (this[onceEventListeners][event]) delete this[onceEventListeners][event];
			if (this[eventListeners][event]) delete this[eventListeners][event];
			this[anyEventListeners] = [];

			return;
		}

		if (event === AnyEvent) {
			const index = this[anyEventListeners].indexOf(listener as EventListener<T, EventKey>);
			this[anyEventListeners].splice(index, 1);
			return;
		}

		const listeners = this[eventListeners][event];
		const onceListeners = this[onceEventListeners][event];

		if (listeners) {
			for (let i = 0; i < listeners.length; i++) {
				const index = listeners.indexOf(listener as EventListener<T, EventKey>);
				if (index > -1) {
					listeners.splice(index, 1);
				}
			}

			if (listeners.length === 0) delete this[eventListeners][event];
		}

		if (onceListeners) {
			for (let i = 0; i < onceListeners.length; i++) {
				const index = onceListeners.indexOf(listener as EventListener<T, EventKey>);
				if (index > -1) {
					onceListeners.splice(index, 1);
				}
			}

			if (onceListeners.length === 0) delete this[onceEventListeners][event];
		}
	}

	public emit<K extends keyof T>(event: K, ...args: EventPayloadArgs<T, K>): boolean {
		const [data] = args;
		let triggered = false;

		if (this[debugKey]) {
			console.log(`Event "${String(event)}" was triggered.`);
			console.log('Data:', data);
		}

		for (const listener of this[anyEventListeners]) {
			triggered = true;
			listener(data as T[K], (event === AnyEvent ? undefined : event) as EventKey);
		}

		if (event === AnyEvent) {
			for (const key in this[eventListeners]) {
				if (this[ignore].has(key)) continue;

				for (let i = 0; i < this[eventListeners][key].length; i++) {
					triggered = true;
					this[eventListeners][key][i](data as T[K]);
				}
			}

			return triggered;
		}

		if (this[ignore].has(event as EventKey)) return triggered;

		if (this[onceEventListeners][event]) {
			for (let i = 0; i < this[eventListeners][event].length; i++) {
				triggered = true;
				this[onceEventListeners][event][i](data as T[K], event as EventKey);
			}
			delete this[onceEventListeners][event];
		}

		if (this[eventListeners][event]) {
			for (let i = 0; i < this[eventListeners][event].length; i++) {
				triggered = true;
				this[eventListeners][event][i](data as T[K], event as EventKey);
			}
		}

		return triggered;
	}

	public unmute<K extends keyof T>(event: K): void {
		if (event === AnyEvent) {
			this[ignore] = new Set();
			return;
		}

		this[ignore].delete(event as EventKey);
	}

	public mute<K extends keyof T>(event: K): void {
		if (event === AnyEvent) {
			this[ignore] = new Set([...Object.keys(this[eventListeners]), ...Object.keys(this[onceEventListeners])]);
			return;
		}

		this[ignore].add(event as EventKey);
	}
}

export default EventEmitter;
export type { EventListener, EventKey, EventsMap, EventPayloadArgs } from './EventEmitter.types';
